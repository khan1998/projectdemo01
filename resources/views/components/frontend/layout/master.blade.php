<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">

    <title>{{$title}}</title>

    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Riode - Ultimate eCommerce Template">
    <meta name="author" content="D-THEMES">

    <!-- Favicon -->
    <link rel="icon" type="image/png" href="{{ asset('ui/frontend') }}/{{ asset('ui/frontend') }}/images/icons/favicon.png">
    <!-- Preload Font -->
    <link rel="preload" href="{{ asset('ui/frontend') }}/fonts/riode.ttf?5gap68" as="font" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" href="{{ asset('ui/frontend') }}/vendor/fontawesome-free/webfonts/fa-solid-900.woff2" as="font" type="font/woff2"
        crossorigin="anonymous">
    <link rel="preload" href="{{ asset('ui/frontend') }}/vendor/fontawesome-free/webfonts/fa-brands-400.woff2" as="font" type="font/woff2"
        crossorigin="anonymous">
    <script>
        WebFontConfig = {
            google: { families: [ 'Poppins:400,500,600,700,800' ] }
        };
        ( function ( d ) {
            var wf = d.createElement( 'script' ), s = d.scripts[ 0 ];
            wf.src = '{{ asset('ui/frontend') }}/js/webfont.js';
            wf.async = true;
            s.parentNode.insertBefore( wf, s );
        } )( document );
    </script>



    <link rel="stylesheet" type="text/css" href="{{ asset('ui/frontend') }}/vendor/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('ui/frontend') }}/vendor/animate/animate.min.css">

    <!-- Plugins CSS File -->
    <link rel="stylesheet" type="text/css" href="{{ asset('ui/frontend') }}/vendor/magnific-popup/magnific-popup.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('ui/frontend') }}/vendor/owl-carousel/owl.carousel.min.css">

    <link rel="stylesheet" type="text/css" href="{{ asset('ui/frontend') }}/vendor/sticky-icon/stickyicon.css">

    <!-- Main CSS File -->
    <link rel="stylesheet" type="text/css" href="{{ asset('ui/frontend') }}/css/demo9.min.css">
    @stack('css')
</head>

<body class="home">

<div class="page-wrapper">
        <h1 class="d-none">Riode - Responsive eCommerce HTML Template</h1>
        
        @include('components.frontend.layout.partials.header')

        <!-- End Header -->
        {{$slot}}
        <!-- End of Main -->
        @include('components.frontend.layout.partials.footer')
  

    <!-- sticky icons-->
	<div class="sticky-icons-wrapper">
		<div class="sticky-icon-links">
			<ul>
				<li><a href="#" class="demo-toggle"><i class="fas fa-home"></i><span>Demos</span></a></li>
				<li><a href="documentation.html"><i class="fas fa-info-circle"></i><span>Documentation</span></a>
				</li>
				<li><a href="https://themeforest.net/downloads/"><i class="fas fa-star"></i><span>Reviews</span></a>
				</li>
				<li><a href="https://d-themes.com/buynow/riodehtml"><i class="fas fa-shopping-cart"></i><span>Buy
							now!</span></a></li>
			</ul>
		</div>
		<div class="demos-list">
			<div class="demos-overlay"></div>
			<a class="demos-close" href="#"><i class="close-icon"></i></a>
			<div class="demos-content scrollable scrollable-light">
				<h3 class="demos-title">Demos</h3>
				<div class="demos">
				</div>
			</div>
		</div>
	</div>
	<!-- Plugins JS File -->
    <script src="{{ asset('ui/frontend') }}/vendor/jquery/jquery.min.js"></script>
    <script src="{{ asset('ui/frontend') }}/vendor/imagesloaded/imagesloaded.pkgd.min.js"></script>
    <script src="{{ asset('ui/frontend') }}/vendor/elevatezoom/jquery.elevatezoom.min.js"></script>
    <script src="{{ asset('ui/frontend') }}/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <script src="{{ asset('ui/frontend') }}/vendor/owl-carousel/owl.carousel.min.js"></script>
    <!-- Main JS File -->
    <script src="{{ asset('ui/frontend') }}/js/main.min.js"></script>
    @stack('js')
</body>

</html>