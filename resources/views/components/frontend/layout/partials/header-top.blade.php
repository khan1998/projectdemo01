<div class="header-top">
                <div class="container">
                    <div class="header-left">
                        <div class="social-links inline-links d-lg-show">
                            <a href="#" title="social-link" class="social-link social-twitter fab fa-twitter"></a>
                            <a href="#" title="social-link" class="social-link social-linkedin fab fa-linkedin-in"></a>
                            <a href="#" title="social-link" class="social-link social-facebook fab fa-facebook-f"></a>
                            <a href="#" title="social-link" class="social-link social-pinterest fab fa-pinterest-p"></a>
                        </div>
                        <p class="welcome-msg">Welcome to Riode store message or remove it!</p>
                    </div>
                    <div class="header-right">
                        <div class="dropdown">
                            <a href="#currency">USD</a>
                            <ul class="dropdown-box">
                                <li><a href="#USD">USD</a></li>
                                <li><a href="#EUR">EUR</a></li>
                            </ul>
                        </div>
                        <!-- End DropDown Menu -->
                        <div class="dropdown ml-4">
                            <a href="#language">ENG</a>
                            <ul class="dropdown-box">
                                <li>
                                    <a href="#USD">ENG</a>
                                </li>
                                <li>
                                    <a href="#EUR">FRH</a>
                                </li>
                            </ul>
                        </div>
                        <!-- End DropDown Menu -->
                        <span class="divider"></span>
                        <a href="contact-us.html" class="contact d-lg-show"><i class="d-icon-map"></i>Contact</a>
                        <a href="#signin" class="login-toggle link-to-tab d-md-show"><i class="d-icon-user"></i>Sign
                            in</a>
                        <span class="delimiter">/</span>
                        <a href="#register" class="register-toggle link-to-tab d-md-show ml-0">Register</a>

                        <div class="dropdown login-dropdown off-canvas">
                            <div class="canvas-overlay"></div>
                            <!-- End Login Toggle -->
                            @include('components.frontend.layout.partials.login')

                            <!-- End Dropdown Box -->
                        </div>
                        <!-- End of Login -->
                    </div>
                </div>
            </div>