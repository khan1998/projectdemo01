
<x-frontend.layout.master>
<x-slot:title>
    Category
</x-slot>
<br>
<body class="riode-rounded-skin">

	<div class="page-wrapper">
		<h1 class="d-none">Riode - Responsive eCommerce HTML Template</h1>

		<!-- End Header -->
		<main class="main shop mt-lg-4">
			<div class="page-header"
				style="background-image: url('{{asset ('ui/frontend') }}/images/demos/demo9/page-header.jpg'); background-color: #E4EAEA;">
				<h1 class="page-title ls-m font-weight-bold">Riode Shop</h1>
				<h3 class="page-subtitle text-uppercase text-white font-weight-normal">16 Products</h3>
			</div>
			<!-- End PageHeader -->
			<div class="page-content mb-10 pb-3">
				<nav class="breadcrumb-nav">
					<div class="container">
						<ul class="breadcrumb">
							<li><a href="demo9.html"><i class="d-icon-home"></i></a></li>
							<li class="active">Riode Shop</li>
						</ul>
					</div>
				</nav>
				<div class="container">
					<nav class="toolbox toolbox-horizontal sticky-toolbox sticky-content fix-top">
						<aside class="sidebar sidebar-fixed shop-sidebar">
							<div class="sidebar-overlay"></div>
							<a class="sidebar-close" href="#"><i class="d-icon-times"></i></a>
							<div class="sidebar-content toolbox-left">
								<div class="toolbox-item select-menu">
									<a class="select-menu-toggle" href="#">Select Size</a>
									<ul class="filter-items">
										<li><a href="#">Extra Large</a></li>
										<li><a href="#">Large</a></li>
										<li><a href="#">Medium</a></li>
										<li><a href="#">Small</a></li>
									</ul>
								</div>
								<div class="toolbox-item select-menu">
									<a class="select-menu-toggle" href="#">Select Color</a>
									<ul class="filter-items">
										<li><a href="#">Black</a></li>
										<li><a href="#">Blue</a></li>
										<li><a href="#">Brown</a></li>
										<li><a href="#">Green</a></li>
									</ul>
								</div>
								<div class="toolbox-item select-menu">
									<a class="select-menu-toggle" href="#">Select Price</a>
									<ul class="filter-items filter-price">
										<li><a href="#">$0.00 - $50.00</a></li>
										<li><a href="#">$50.00 - $100.00</a></li>
										<li><a href="#">$100.00 - $200.00</a></li>
										<li><a href="#">$200.00+</a></li>
									</ul>
								</div>
							</div>
						</aside>
						<div class="toolbox-left">
							<a href="#"
								class="toolbox-item left-sidebar-toggle btn btn-sm btn-outline btn-primary btn-rounded btn-icon-right d-lg-none">
								Filter<i class="d-icon-arrow-right"></i></a>
							<div class="toolbox-item toolbox-sort select-menu">
								<select name="orderby" class="form-control">
									<option value="default" selected="selected">Default Sorting</option>
									<option value="popularity">Most Popular</option>
									<option value="rating">Average rating</option>
									<option value="date">Latest</option>
									<option value="price-low">Sort forward price low</option>
									<option value="price-high">Sort forward price high</option>
									<option value="">Clear custom sort</option>
								</select>
							</div>
						</div>
						<div class="toolbox-right">
							<div class="toolbox-item toolbox-show select-box text-dark">
								<label>show :</label>
								<select name="count" class="form-control">
									<option value="12">12</option>
									<option value="24">24</option>
									<option value="36">36</option>
								</select>
							</div>
							<div class="toolbox-item toolbox-layout">
								<a href="shop-list-mode.html" class="d-icon-mode-list btn-layout"></a>
								<a href="shop.html" class="d-icon-mode-grid btn-layout active"></a>
							</div>
						</div>
					</nav>
					<div class="select-items">
						<a href="#" class="filter-clean text-primary">Clean All</a>
					</div>
					<div class="row cols-2 cols-sm-3 cols-md-4 product-wrapper">
						<div class="product-wrap">
							<div class="product text-center">
								<figure class="product-media">
									<a href="{{url('product/details')}}">
										<img src="{{asset ('ui/frontend') }}/images/demos/demo9/products/1.jpg" alt="product" width="300"
											height="338">
										<img src="{{asset ('ui/frontend') }}/images/demos/demo9/products/1-1.jpg" alt="product" width="300"
											height="338">
									</a>
									<div class="product-label-group">
										<label class="product-label label-new">new</label>
									</div>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
											View</a>
									</div>
								</figure>
								<div class="product-details">
									<div class="product-cat">
										<a href="demo9-shop.html">Clothing</a>
									</div>
									<h3 class="product-name">
										<a href="{{url('product/details')}}">Converse Season Shoes</a>
									</h3>
									<div class="product-price">
										<ins class="new-price">$199.00</ins><del class="old-price">$210.00</del>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:100%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="{{url('product/details')}}" class="rating-reviews">( 6 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product text-center">
								<figure class="product-media">
									<a href="{{url('product/details')}}">
										<img src="{{asset ('ui/frontend') }}/images/demos/demo9/products/2.jpg" alt="product" width="300"
											height="338">
										<img src="{{asset ('ui/frontend') }}/images/demos/demo9/products/2-1.jpg" alt="product" width="300"
											height="338">
									</a>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
											View</a>
									</div>
								</figure>
								<div class="product-details">
									<div class="product-cat">
										<a href="demo9-shop.html">bags</a>
									</div>
									<h3 class="product-name">
										<a href="{{url('product/details')}}">Man's Grey Wrist Watch</a>
									</h3>
									<div class="product-price">
										<span class="price">$35.00</span>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:100%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="{{url('product/details')}}" class="rating-reviews">( 6 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product text-center">
								<figure class="product-media">
									<a href="{{url('product/details')}}">
										<img src="{{asset ('ui/frontend') }}/images/demos/demo9/products/3.jpg" alt="product" width="300"
											height="338">
										<img src="{{asset ('ui/frontend') }}/images/demos/demo9/products/3-1.jpg" alt="product" width="300"
											height="338">
									</a>

									<div class="product-label-group">
										<label class="product-label label-sale">27% off</label>
									</div>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
											View</a>
									</div>
								</figure>
								<div class="product-details">
									<div class="product-cat">
										<a href="demo9-shop.html">electric</a>
									</div>
									<h3 class="product-name">
										<a href="{{url('product/details')}}">Hand Electric Cell</a>
									</h3>
									<div class="product-price">
										<span class="price">$19.00</span>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:100%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="{{url('product/details')}}" class="rating-reviews">( 6 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product text-center">
								<figure class="product-media">
									<a href="{{url('product/details')}}">
										<img src="{{asset ('ui/frontend') }}/images/demos/demo9/products/4.jpg" alt="product" width="300"
											height="338">
										<img src="{{asset ('ui/frontend') }}/images/demos/demo9/products/4-1.jpg" alt="product" width="300"
											height="338">
									</a>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
											View</a>
									</div>
								</figure>
								<div class="product-details">
									<div class="product-cat">
										<a href="demo9-shop.html">Clothing</a>
									</div>
									<h3 class="product-name">
										<a href="{{url('product/details')}}">Men's Fashion Jacket</a>
									</h3>
									<div class="product-price">
										<ins class="new-price">$98.00</ins><del class="old-price">$210.00</del>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:100%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="{{url('product/details')}}" class="rating-reviews">( 6 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product text-center">
								<figure class="product-media">
									<a href="{{url('product/details')}}">
										<img src="{{asset ('ui/frontend') }}/images/demos/demo9/products/6.jpg" alt="product" width="300"
											height="338">
										<img src="{{asset ('ui/frontend') }}/images/demos/demo9/products/6-1.jpg" alt="product" width="300"
											height="338">
									</a>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
											View</a>
									</div>
								</figure>
								<div class="product-details">
									<div class="product-cat">
										<a href="demo9-shop.html">accessories</a>
									</div>
									<h3 class="product-name">
										<a href="{{url('product/details')}}">Men's Winter Hood</a>
									</h3>
									<div class="product-price">
										<ins class="new-price">$98.00</ins><del class="old-price">$210.00</del>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:100%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="{{url('product/details')}}" class="rating-reviews">( 6 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product text-center">
								<figure class="product-media">
									<a href="{{url('product/details')}}">
										<img src="{{asset ('ui/frontend') }}/images/demos/demo9/products/7.jpg" alt="product" width="300"
											height="338">
										<img src="{{asset ('ui/frontend') }}/images/demos/demo9/products/7-1.jpg" alt="product" width="300"
											height="338">
									</a>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
											View</a>
									</div>
								</figure>
								<div class="product-details">
									<div class="product-cat">
										<a href="demo9-shop.html">shoes</a>
									</div>
									<h3 class="product-name">
										<a href="{{url('product/details')}}">Women's Comforter</a>
									</h3>
									<div class="product-price">
										<ins class="new-price">$98.00</ins><del class="old-price">$210.00</del>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:100%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="{{url('product/details')}}" class="rating-reviews">( 6 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product text-center">
								<figure class="product-media">
									<a href="{{url('product/details')}}">
										<img src="{{asset ('ui/frontend') }}/images/demos/demo9/products/11.jpg" alt="product" width="300"
											height="338">
									</a>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
											View</a>
									</div>
								</figure>
								<div class="product-details">
									<div class="product-cat">
										<a href="demo9-shop.html">Clothes</a>
									</div>
									<h3 class="product-name">
										<a href="{{url('product/details')}}">Converse Blue Training Shoes</a>
									</h3>
									<div class="product-price">
										<ins class="new-price">$98.00</ins><del class="old-price">$210.00</del>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:100%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="{{url('product/details')}}" class="rating-reviews">( 6 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product text-center">
								<figure class="product-media">
									<a href="{{url('product/details')}}">
										<img src="{{asset ('ui/frontend') }}/images/demos/demo9/products/8.jpg" alt="product" width="300"
											height="338">
										<img src="{{asset ('ui/frontend') }}/images/demos/demo9/products/8-1.jpg" alt="product" width="300"
											height="338">
									</a>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
											View</a>
									</div>
								</figure>
								<div class="product-details">
									<div class="product-cat">
										<a href="demo9-shop.html">Watches</a>
									</div>
									<h3 class="product-name">
										<a href="{{url('product/details')}}">Blue Comfortable Cup</a>
									</h3>
									<div class="product-price">
										<ins class="new-price">$98.00</ins><del class="old-price">$210.00</del>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:100%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="{{url('product/details')}}" class="rating-reviews">( 6 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product text-center">
								<figure class="product-media">
									<a href="{{url('product/details')}}">
										<img src="{{asset ('ui/frontend') }}/images/demos/demo9/products/9.jpg" alt="product" width="300"
											height="338">
									</a>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
											View</a>
									</div>
								</figure>
								<div class="product-details">
									<div class="product-cat">
										<a href="demo9-shop.html">Women’s</a>
									</div>
									<h3 class="product-name">
										<a href="{{url('product/details')}}">Leather Belt</a>
									</h3>
									<div class="product-price">
										<ins class="new-price">$98.00</ins><del class="old-price">$210.00</del>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:100%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="{{url('product/details')}}" class="rating-reviews">( 6 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product text-center">
								<figure class="product-media">
									<a href="{{url('product/details')}}">
										<img src="{{asset ('ui/frontend') }}/images/demos/demo9/products/12.jpg" alt="product" width="300"
											height="338">
									</a>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
											View</a>
									</div>
								</figure>
								<div class="product-details">
									<div class="product-cat">
										<a href="demo9-shop.html">shoes</a>
									</div>
									<h3 class="product-name">
										<a href="{{url('product/details')}}">Spon wide-striped shirt</a>
									</h3>
									<div class="product-price">
										<ins class="new-price">$98.00</ins><del class="old-price">$210.00</del>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:100%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="{{url('product/details')}}" class="rating-reviews">( 6 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product text-center">
								<figure class="product-media">
									<a href="{{url('product/details')}}">
										<img src="{{asset ('ui/frontend') }}/images/demos/demo9/products/10.jpg" alt="product" width="300"
											height="338">
									</a>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
											View</a>
									</div>
								</figure>
								<div class="product-details">
									<div class="product-cat">
										<a href="demo9-shop.html">Women’s</a>
									</div>
									<h3 class="product-name">
										<a href="{{url('product/details')}}">Cavin Fashion Suede Handbag</a>
									</h3>
									<div class="product-price">
										<ins class="new-price">$98.00</ins><del class="old-price">$210.00</del>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:100%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="{{url('product/details')}}" class="rating-reviews">( 6 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product text-center">
								<figure class="product-media">
									<a href="{{url('product/details')}}">
										<img src="{{asset ('ui/frontend') }}/images/demos/demo9/products/13.jpg" alt="product" width="300"
											height="338">
									</a>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
											View</a>
									</div>
								</figure>
								<div class="product-details">
									<div class="product-cat">
										<a href="demo9-shop.html">electronics</a>
									</div>
									<h3 class="product-name">
										<a href="{{url('product/details')}}">Women’s Beautiful Headgear</a>
									</h3>
									<div class="product-price">
										<ins class="new-price">$98.00</ins><del class="old-price">$210.00</del>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:100%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="{{url('product/details')}}" class="rating-reviews">( 6 reviews )</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<nav class="toolbox toolbox-pagination">
						<p class="toolbox-item show-info d-block">Showing 1-12 of 56 Products</p>
						<ul class="pagination">
							<li class="page-item disabled">
								<a class="page-link page-link-prev" href="#" aria-label="Previous" tabindex="-1"
									aria-disabled="true">
									<i class="d-icon-arrow-left"></i>Prev
								</a>
							</li>
							<li class="page-item active" aria-current="page"><a class="page-link" href="#">1</a></li>
							<li class="page-item"><a class="page-link" href="#">2</a></li>
							<li class="page-item"><a class="page-link" href="#">3</a></li>
							<li class="page-item">
								<a class="page-link page-link-next" href="#" aria-label="Next">
									Next<i class="d-icon-arrow-right"></i>
								</a>
							</li>
						</ul>
					</nav>
				</div>
			</div>
			
		</main>
		<!-- End of Main -->

	<!-- Plugins JS File -->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/imagesloaded/imagesloaded.pkgd.min.js"></script>
	<script src="vendor/elevatezoom/jquery.elevatezoom.min.js"></script>
	<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

	<script src="vendor/owl-carousel/owl.carousel.min.js"></script>
	<!-- Main JS File -->
	<script src="js/main.min.js"></script>
</body>



</x-frontend.layout.master>