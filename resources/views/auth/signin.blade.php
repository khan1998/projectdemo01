

  <!-- Custom styles for Sign-Up template -->  
  

<x-frontend.layout.master>

<x-slot:title>
  Authorization
</x-slot>

<br>
<div class="cont" >
  <div class="form sign-in">
    <h2>Welcome</h2>
    <label>
      <span>Email</span>
      <input type="email" />
    </label>
    <label>
      <span>Password</span>
      <input type="password" />
    </label>
    <p class="forgot-pass"><a href="">Forgot password?</a> </p>
    <a href="{{ route('admin.dashboard') }}"> <button type="button" class="submit" >Sign In </button></a>
    <!-- <a class="btn btn-primary" href="{{ route('medicine.medicine') }}">Login</a> -->

  </div>
  <div class="sub-cont">
    <div class="img">
      <div class="img__text m--up">

        <h3>Don't have an account? Please Sign up!<h3>
      </div>
      <div class="img__text m--in">

        <h3>If you already has an account, just sign in.<h3>
      </div>
      <div class="img__btn">
        <span class="m--up">Sign Up</span>
        <span class="m--in">Sign In</span>
      </div>
    </div>
    <div class="form sign-up">
      <h2>Create your Account</h2>
      <label>
        <span>Name</span>
        <input type="text" />
      </label>
      <label>
        <span>Email</span>
        <input type="email" />
      </label>
      <label>
        <span>Password</span>
        <input type="password" />
      </label>
      <a href="{{ route('admin.signin') }}"> <button type="button" class="submit">Sign Up</button> </a>

    </div>
  </div>
</div>
@push('css')
<link rel="stylesheet" href="{{ asset('ui/auth') }}/css/signup.css"/>
@endpush()

<script>
  document.querySelector('.img__btn').addEventListener('click', function() {
    document.querySelector('.cont').classList.toggle('s--signup');
  });
</script>

</x-frontend.layout.master>