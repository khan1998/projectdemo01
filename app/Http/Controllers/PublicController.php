<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function home()
    {
        return view('public.homepage');
    }
    public function details()
    {
        return view('public.details');
    }
    public function buy()
    {
        return view('public.buypage');
    }
    public function cart()
    {
        return view('public.cart');
    }
    public function checkout()
    {
        return view('public.checkout');
    }
    public function category()
    {
        return view('public.category');
    }
}
