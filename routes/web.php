<?php
use App\Http\Controllers\PublicController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\AuthController;



use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::prefix('')->group(function(){
    Route::get('/', [PublicController::class, 'home']);
    Route::get('/product/details', [PublicController::class, 'details']);
    Route::get('/category', [PublicController::class, 'category']);
    Route::get('/checkout', [PublicController::class, 'checkout']);
    Route::get('/buy', [PublicController::class, 'buy'])->name('buy.buy');
    Route::get('/cart', [PublicController::class, 'cart']);
    Route::get('/authorization', [AuthController::class, 'signin'])->name('admin.signin');


});

Route::prefix('admin')->group(function(){
    Route::get('/', [AdminController::class, 'dashboard'])->name('admin.dashboard');
    Route::get('/products/list', [ProductController::class, 'index'])->name('products.index');
    Route::get('/products/create', [ProductController::class, 'create'])->name('products.create');




});




